// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include "analyzer/parallel/ps_pool/ps_pool.h"

#include <asm-generic/errno.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <unistd.h>

#include "common/utils/utils.h"

// ----------- PRIVATE -----------//

// NOLINTNEXTLINE (*-global-variables)
static volatile bool shutdown = false;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
// NOLINTNEXTLINE
void sig_handler_child(int signum) {
#pragma GCC diagnostic pop
  shutdown = true;
}

void forked_main(int msgqid, size_t dsize, child_scope_t *scope,
                 void (*func)(child_scope_t *scope, void *req_data,
                              void *resp_data)) {
  while (!shutdown) {
    ps_msgbuf_t req = {.mtype = TASK_MSG}, resp = {.mtype = RESULT_MSG};
    // the call for queue is interrupted when signal is catched
    if (msgrcv(msgqid, (void *)&req, dsize, TASK_MSG, 0) == -1) {
      continue;
    }

    func(scope, req.mtext, resp.mtext);
    if (msgsnd(msgqid, (void *)&resp, dsize, 0) == -1) {
      continue;
    }
  }
}

void kill_processes(int *pids, size_t ps_num) {
  for (size_t j = 0; j < ps_num; ++j) {
    kill(pids[j], SIGUSR1);
    waitpid(pids[j], NULL, 0);
  }
}

void free_mem_and_queue(ps_pool_t *pool) {
  if (pool->cscope->shared_mem) {
    munmap(pool->cscope->shared_mem, pool->cscope->shared_mem_size);
  }

  free(pool->cscope);
  msgctl(pool->msgqid, IPC_RMID, NULL);

  free(pool->pids);
  free(pool);
}

// ----------- PUBLIC -----------//

ps_pool_t *create_ps_pool(void (*func)(child_scope_t *scope, void *req_data,
                                       void *resp_data),
                          size_t dsize) {
  long tmp_ps_num = sysconf(_SC_NPROCESSORS_CONF);
  if (tmp_ps_num == -1) {
    return NULL;
  }

  ps_pool_t *pool = malloc(sizeof(ps_pool_t));
  if (!pool) {
    return NULL;
  }

  pool->cscope = malloc(sizeof(child_scope_t));
  if (!pool->cscope) {
    free(pool);
    return NULL;
  }

  pool->data_size = dsize;
  // NOLINTNEXTLINE (*-magic-numbers)
  if ((pool->msgqid = msgget(IPC_PRIVATE, IPC_CREAT | 0660)) == -1) {
    free(pool->cscope);
    free(pool);
    return NULL;
  }

  // pool->worker_main = func;
  pool->ps_num = (size_t)tmp_ps_num;
  pool->cscope->shared_mem_size = MAX_BUFFER_SIZE;
  pool->cscope->shared_mem =
      mmap(NULL, pool->cscope->shared_mem_size, PROT_READ | PROT_WRITE,
           MAP_SHARED | MAP_ANONYMOUS, 0, 0);

  // NOLINTNEXTLINE (*-no-int-to-ptr)
  if (pool->cscope->shared_mem == MAP_FAILED) {
    free(pool->cscope);
    free(pool);
    return NULL;
  }

  pool->pids = calloc(pool->ps_num, sizeof(int));
  if (!pool->pids) {
    munmap(pool->cscope->shared_mem, pool->cscope->shared_mem_size);
    free(pool->cscope);

    msgctl(pool->msgqid, IPC_RMID, NULL);
    free(pool);
    return NULL;
  }

  for (size_t i = 0; i < pool->ps_num; ++i) {
    pool->pids[i] = fork();
    if (pool->pids[i] == -1) {
      for (size_t j = 0; j < i; ++j) {
        kill(pool->pids[j], SIGUSR1);
      }

      munmap(pool->cscope->shared_mem, pool->cscope->shared_mem_size);
      free(pool->cscope);

      for (size_t j = 0; j < i; ++j) {
        waitpid(pool->pids[j], NULL, 0);
      }

      msgctl(pool->msgqid, IPC_RMID, NULL);
      free(pool->pids);
      free(pool);
      return NULL;
    }

    if (pool->pids[i] == 0) {
      signal(SIGUSR1, sig_handler_child);
      forked_main(pool->msgqid, pool->data_size, pool->cscope, func);
      free_mem_and_queue(pool);
      // NOLINTNEXTLINE (*-unsafe)
      exit(0);
    }
  }

  return pool;
}

void share_memory(ps_pool_t *pool, const char *const mem, size_t mem_size) {
  memset(pool->cscope->shared_mem, 0, pool->cscope->shared_mem_size);
  memcpy(pool->cscope->shared_mem, mem, mem_size);
}

int enqueue(ps_pool_t *pool, void *task) {
  ps_msgbuf_t req = {.mtype = TASK_MSG};
  memcpy(req.mtext, task, pool->data_size);
  if (msgsnd(pool->msgqid, &req, pool->data_size, 0) == -1) {
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

void get_result(ps_pool_t *pool, void *result) {
  ps_msgbuf_t res = {.mtype = RESULT_MSG};
  if (msgrcv(pool->msgqid, (void *)&res, pool->data_size, RESULT_MSG, 0) < 0) {
    return;
  }

  memcpy(result, res.mtext, pool->data_size);
}

void free_ps_pool(ps_pool_t *pool) {
  kill_processes(pool->pids, pool->ps_num);
  free_mem_and_queue(pool);
}
