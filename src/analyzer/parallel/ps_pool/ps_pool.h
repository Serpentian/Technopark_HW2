// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#ifndef SRC_ANALYZER_PARALLEL_PS_POOL_PS_POOL_H_
#define SRC_ANALYZER_PARALLEL_PS_POOL_PS_POOL_H_

#include <stdbool.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>

#define MAX_MSG_SIZE 4092

typedef struct {
  size_t shared_mem_size;
  char *shared_mem;
} child_scope_t;

typedef struct {
  int *pids;
  size_t data_size;
  size_t ps_num;
  int msgqid;
  child_scope_t *cscope;
} ps_pool_t;

typedef struct {
  long mtype;
  char mtext[MAX_MSG_SIZE];
} ps_msgbuf_t;

enum msg_type { TASK_MSG = 1, RESULT_MSG };

// func - worker function, dsize - size of the data
ps_pool_t *create_ps_pool(void (*func)(child_scope_t *scope, void *req_data,
                                       void *resp_data),
                          size_t dsize);

void share_memory(ps_pool_t *pool, const char *mem, size_t mem_size);
int enqueue(ps_pool_t *pool, void *task);
void free_ps_pool(ps_pool_t *pool);

// blocking call
void get_result(ps_pool_t *pool, void *result);

#endif  // SRC_ANALYZER_PARALLEL_PS_POOL_PS_POOL_H_
