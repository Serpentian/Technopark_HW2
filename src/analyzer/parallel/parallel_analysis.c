// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include <stdlib.h>
#include <string.h>
#include <sys/param.h>

#include "analyzer/analyzer.h"
#include "analyzer/parallel/ps_pool/ps_pool.h"
#include "common/utils/utils.h"

#define PER_PROCESS_MAX_DATA_SIZE (MAX_BUFFER_SIZE / 20)
#define RIGHT_CHUNK_BOUND ((tasks_num + 1) * PER_PROCESS_MAX_DATA_SIZE)

typedef struct {
  size_t first;
  size_t second;
} data_t;

void worker(child_scope_t *scope, void *req_data, void *resp_data) {
  data_t *data = req_data;
  count_result_t res =
      count_digraphs(scope->shared_mem, data->first, data->second);
  memcpy(resp_data, &res, sizeof(count_result_t));
}

int analyze(analyzer_t *analyzer) {
  size_t buf_size = (size_t)get_buf_size(analyzer->options->input);
  ps_pool_t *pool = create_ps_pool(worker, sizeof(data_t));
  if (!pool) {
    return EXIT_FAILURE;
  }

  char *buffer = malloc(buf_size);
  size_t read_size = 0;

  do {
    read_size = fread(buffer, sizeof(char), buf_size, analyzer->options->input);
    if (read_size != buf_size && !feof(analyzer->options->input)) {
      free(buffer);
      free_ps_pool(pool);
      return EXIT_FAILURE;
    }

    share_memory(pool, buffer, read_size);
    size_t chunk_bound = 0, tasks_num = 0;
    do {
      chunk_bound = MIN(RIGHT_CHUNK_BOUND, buf_size);
      data_t task = {tasks_num * PER_PROCESS_MAX_DATA_SIZE, chunk_bound};
      enqueue(pool, (void *)&task);

      // processing the chunk boundary digraph
      if (chunk_bound != MIN(MAX_BUFFER_SIZE, buf_size) &&
          buffer[chunk_bound - 1] == ':') {
        switch (buffer[chunk_bound]) {
          case ')':
            ++analyzer->positive_num;
            break;

          case '(':
            ++analyzer->negative_num;
            break;
        }
      }

      ++tasks_num;
    } while (chunk_bound != MIN(buf_size, MAX_BUFFER_SIZE));

    if (tasks_num) {
      data_t *result = malloc(sizeof(data_t) * tasks_num);
      for (size_t i = 0; i < tasks_num; ++i) {
        get_result(pool, &result[i]);
        analyzer->positive_num += result[i].first;
        analyzer->negative_num += result[i].second;
      }

      free(result);
    }
  } while (read_size == MAX_BUFFER_SIZE);

  free_ps_pool(pool);
  free(buffer);
  return EXIT_SUCCESS;
}
