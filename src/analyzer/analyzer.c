// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include "analyzer/analyzer.h"

#include <string.h>

#define MAX_SIZE_OF_RES_STR 9

void print_result(analyzer_t *analyzer) {
  char result[MAX_SIZE_OF_RES_STR];
  memset(result, '\0', sizeof(result));

  if (analyzer->positive_num > analyzer->negative_num) {
    strncpy(result, "Positive", MAX_SIZE_OF_RES_STR);
  } else if (analyzer->positive_num < analyzer->negative_num) {
    strncpy(result, "Negative", MAX_SIZE_OF_RES_STR);
  } else {
    strncpy(result, "Neutral", MAX_SIZE_OF_RES_STR);
  }

  fprintf(analyzer->options->output, "Number of positive digraphs: %zu\n",
          analyzer->positive_num);
  fprintf(analyzer->options->output, "Number of negative digraphs: %zu\n",
          analyzer->negative_num);
  fprintf(analyzer->options->output, "The emotional coloring is '%s'\n",
          result);
}
