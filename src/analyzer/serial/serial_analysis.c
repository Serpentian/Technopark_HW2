// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include <stdlib.h>

#include "analyzer/analyzer.h"
#include "common/utils/utils.h"

#define ANALYZER_INPUT analyzer->options->input

int analyze(analyzer_t *analyzer) {
  size_t buf_size = 0;
  if (!(buf_size = (size_t)get_buf_size(ANALYZER_INPUT))) {
    return EXIT_FAILURE;
  }

  char *buffer = malloc(buf_size);
  size_t read = 0;
  // not loading the whole file in RAM
  while ((read = fread(buffer, sizeof(char), buf_size, ANALYZER_INPUT))) {
    if (read != buf_size && !feof(ANALYZER_INPUT)) {
      free(buffer);
      return EXIT_FAILURE;
    }

    count_result_t count = count_digraphs(buffer, 0, read);
    analyzer->negative_num += count.nnum;
    analyzer->positive_num += count.pnum;
  }

  free(buffer);
  return EXIT_SUCCESS;
}
