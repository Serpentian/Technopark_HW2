// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#ifndef SRC_ANALYZER_ANALYZER_H_
#define SRC_ANALYZER_ANALYZER_H_

#include <stdlib.h>

#include "common/options.h"

typedef struct {
  size_t positive_num;
  size_t negative_num;
  options_t *options;
  // on 64-bit systems all of structure members will be aligned to 8
} __attribute__((aligned(sizeof(size_t)))) analyzer_t;

int analyze(analyzer_t *analyzer);
void print_result(analyzer_t *analyzer);

#endif  // SRC_ANALYZER_ANALYZER_H_
