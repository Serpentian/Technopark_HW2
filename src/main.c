// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include <errno.h>
#include <getopt.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef ENABLE_BENCHMARK
#include <time.h>
#endif

#include "analyzer/analyzer.h"
#include "common/options.h"
#include "common/utils/utils.h"

#define SYS_ERR "System error"
#define DEFAULT_PROGNAME "msg-analysys"
#define NANOSEC_IN_SEC 1e9

// no need to test these functions
void help(char *progname);
int run(options_t *options);

int main(int argc, char *argv[]) {
  // default options, input is initialized from
  // the arguments or gotten from a tmp file
  options_t options = {.output = stdout, .input = NULL};

  // the endless loop is the way GNU docs
  // recommends to use getopt_long method
  while (true) {
    static struct option long_options[] = {
        // not using the options which can set a flag because it's
        // impossible to define a short version of an argument for them
        {"help", no_argument, 0, 'h'},
        {"input", required_argument, 0, 'i'},
        {"output", required_argument, 0, 'o'},
        {0, 0, 0, 0}};

    int option_idx = 0;
    // NOLINTNEXTLINE: concurrency unsafe function
    int opt = getopt_long(argc, argv, "hi:o:", long_options, &option_idx);

    // detect the end of the options
    if (opt == -1) {
      break;
    }

    switch (opt) {
      case 'i':
        if (!(options.input = fopen(optarg, "rb"))) {
          perror(SYS_ERR);
          return EXIT_FAILURE;
        }

        break;

      case 'o':
        if (!(options.output = fopen(optarg, "wb"))) {
          perror(SYS_ERR);
          return EXIT_FAILURE;
        }

        break;

      case 'h':
      default:
        help(basename(argv[0]));
        return EXIT_FAILURE;
    }
  }

  if (optind < argc) {
    fprintf(stderr, "Warning: unknown arguments encountered: ");
    while (optind < argc - 1) {
      fprintf(stderr, "%s, ", argv[optind++]);
    }

    fprintf(stderr, "%s\n", argv[optind]);
  }

  FILE *tmp_file = NULL;
  // interactive mode
  if (!options.input) {
    tmp_file = write_str(stdin);
    options.input = tmp_file;
    if (!options.input) {
      perror(SYS_ERR);
      return EXIT_FAILURE;
    }
  }

  if (run(&options) != EXIT_SUCCESS) {
    perror(SYS_ERR);
    return EXIT_FAILURE;
  }

  if (tmp_file) {
    fclose(tmp_file);
  }

  return EXIT_SUCCESS;
}

void help(char *progname) {
  printf("Usage: %s [OPTION]...\n", progname ? progname : DEFAULT_PROGNAME);
  printf("Determine the emotional coloring of the correspondence\n\n");
  printf(
      "Mandatory arguments to long options"
      "are mandatory for short options too.\n");
  printf("  -i, --input=[FILE]    get the messages from the FILE\n");
  printf("  -o, --output=[FILE]   output all info to the FILE\n");
  printf("  -h, --help            display this help and exit\n");
}

int run(options_t *options) {
  analyzer_t analyzer = {
      .negative_num = 0, .positive_num = 0, .options = options};

#ifdef ENABLE_BENCHMARK
  struct timespec ts_start = {.tv_sec = 0, .tv_nsec = 0};
  struct timespec ts_stop = {.tv_sec = 0, .tv_nsec = 0};
  if (clock_gettime(CLOCK_REALTIME, &ts_start) == -1) {
    return EXIT_FAILURE;
  }
#endif

  if (analyze(&analyzer) != EXIT_SUCCESS) {
    return EXIT_FAILURE;
  }

#ifdef ENABLE_BENCHMARK
  if (clock_gettime(CLOCK_REALTIME, &ts_stop) == -1) {
    return EXIT_FAILURE;
  }
  double time = (double)(ts_stop.tv_sec - ts_start.tv_sec) +
                (double)(ts_stop.tv_nsec - ts_start.tv_nsec) / NANOSEC_IN_SEC;
#endif

  print_result(&analyzer);

#ifdef ENABLE_BENCHMARK
  fprintf(options->output, "Analysis took %lf seconds\n", time);
#endif

  return EXIT_SUCCESS;
}
