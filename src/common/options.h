// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#ifndef SRC_COMMON_OPTIONS_H_
#define SRC_COMMON_OPTIONS_H_

#include <stdbool.h>
#include <stdio.h>

typedef struct {
  FILE *output;
  FILE *input;
} __attribute__((__aligned__(sizeof(FILE *)))) options_t;

#endif  // SRC_COMMON_OPTIONS_H_
