#include "common/utils/utils.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
  char *str;
  size_t str_size;
  size_t str_cap;
} buffer_t;

// --- Private --- //
int dump_to_file(FILE *tmp, buffer_t *buffer) {
  size_t num = fwrite(buffer->str, sizeof(char), buffer->str_size, tmp);
  if (num != buffer->str_size) {
    return EXIT_FAILURE;
  }

  free(buffer->str);
  buffer->str_size = 0;
  buffer->str_cap = 0;
  return EXIT_SUCCESS;
}

// --- Public --- //

// NOLINTNEXTLINE (readability-function-cognitive-complexity)
FILE *write_str(FILE *input) {
  if (!input) {
    return NULL;
  }

  buffer_t buffer = {NULL, 0, 0};
  FILE *tmp = tmpfile();

  if (tmp) {
    for (int c = fgetc(input); c != EOF; c = fgetc(input)) {
      if (buffer.str_size + 1 >= buffer.str_cap) {
        size_t new_cap = buffer.str_cap ? buffer.str_cap * 2 : 1;
        if (new_cap > MAX_BUFFER_SIZE) {
          if (dump_to_file(tmp, &buffer) != EXIT_SUCCESS) {
            free(buffer.str);
            fclose(tmp);
            return NULL;
          }

          new_cap = 1;
        }

        char *temp = (char *)realloc(buffer.str, sizeof(char) * (new_cap + 1));

        if (temp) {
          buffer.str = temp;
          buffer.str_cap = new_cap;
        } else {
          free(buffer.str);
          return NULL;
        }
      }

      buffer.str[buffer.str_size++] = (char)c;
      buffer.str[buffer.str_size] = '\0';
    }

    if (dump_to_file(tmp, &buffer) != EXIT_SUCCESS) {
      free(buffer.str);
      fclose(tmp);
      return NULL;
    }

    rewind(tmp);
  }

  return tmp;
}

count_result_t count_digraphs(const char *const data, size_t start_idx,
                              size_t end_idx) {
  count_result_t res = {.nnum = 0, .pnum = 0};
  bool is_prev_colon = false;  // yep, like in Turing machine
  for (size_t i = start_idx; i < end_idx; ++i) {
    if (data[i] == ':') {
      is_prev_colon = true;
    } else if (is_prev_colon) {
      if (data[i] == ')') {
        ++res.pnum;
      } else if (data[i] == '(') {
        ++res.nnum;
      }

      is_prev_colon = false;
    }
  }

  return res;
}

long file_size(FILE *file) {
  fseek(file, 0L, SEEK_END);
  long size = ftell(file);
  rewind(file);
  return size;
}

long get_buf_size(FILE *file) {
  long fsize = file_size(file);
  if (fsize == -1L) {
    return 0;
  }

  return fsize < MAX_BUFFER_SIZE ? fsize : MAX_BUFFER_SIZE;
}
