// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#ifndef SRC_COMMON_UTILS_UTILS_H_
#define SRC_COMMON_UTILS_UTILS_H_

#include <stdio.h>

#define MAX_BUFFER_SIZE 200000000  // approx 200 megabytes

typedef struct {
  size_t pnum;
  size_t nnum;
} count_result_t;

// writes str from stdin to tmpfile
FILE *write_str(FILE *input);
// find in range [start_idx, end_idx) all digraphs
count_result_t count_digraphs(const char *data, size_t start_idx,
                              size_t end_idx);
long file_size(FILE *file);

// either file size or MAX_BUFFER_SIZE
long get_buf_size(FILE *file);

#endif  // SRC_COMMON_UTILS_UTILS_H_
