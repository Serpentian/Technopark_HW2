#!/bin/bash

function usage {
    echo -e "\nUsage: $(basename $0) [OPTION]..."
    echo -e "Generate message log with diagraphs\n"
    echo "-h          display this help and exit"
    echo "-n NUM      generate NUM messages"
    exit 1
}

while getopts "hn:" o; do
    case "${o}" in
        n)
            msg_num=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

if [ -z "${msg_num}" ]; then
    msg_num=50;
fi

for (( i=0 ; i<$msg_num ; i++ )); do
    base64 /dev/urandom | \
        sed 's/[^[:alpha:]]/ /g' |
        awk -v rnd=$RANDOM '{
        if(rnd % 3 == 0) {
            print $0":)"
        } else if(rnd % 3 == 1) {
            print $0":("
        } else {
            print $0
        }}' | \
        head -1 | \
        awk '{"date \"+[%x %X]\"" | getline d; print d, $0}'
done
