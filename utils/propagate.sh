#!/bin/bash

function usage {
    echo -e "\nUsage: $(basename $0) FILE NEW_SIZE"
    echo -e "Concatanate the content of the FILE with itself\n"
    echo -e "while the size of the FILE is less then NEW_SIZE\n"
    echo "-h          display this help and exit"
    exit 1
}

while getopts "h" o; do
    case "${o}" in
        *)
            usage
            ;;
    esac
done

if [ "$#" -ne 2 ]; then
  usage
fi

file_name=$1
new_file_size=$2

tmpfile="tmp_file_name"
touch $tmpfile

while [ $(du $tmpfile | awk '{print $1}') -le "$new_file_size" ]; do
  cat $file_name >> "$tmpfile"
done

mv $tmpfile $file_name
