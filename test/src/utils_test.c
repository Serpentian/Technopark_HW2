// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common/utils/utils.h"

static void write_str_test(void **state) {
  char input_str[] = "Hello, World";
  FILE *input = fmemopen(input_str, sizeof(input_str), "r");

  FILE *output = write_str(input);
  char output_str[sizeof(input_str)];
  fread(output_str, sizeof(char), sizeof(output_str), output);

  assert_string_equal(input_str, output_str);

  fclose(input);
  fclose(output);
  (void)state;
}

static void count_digraphs_test(void **state) {
  char input_str[] = ";) fdsjfs :):) sdjfsi :(";
  count_result_t res = count_digraphs(input_str, 0, sizeof(input_str));
  assert_int_equal(res.pnum, 2);
  assert_int_equal(res.nnum, 1);
  (void)state;
}

static void file_size_test(void **state) {
  char input_str[10] = "0123456789";
  FILE *input = fmemopen(input_str, sizeof(input_str), "r");
  long size = file_size(input);
  assert_int_equal(size, sizeof(input_str));  // no \0 file read
  fclose(input);
  (void)state;
}

static void get_buf_size_test(void **state) {
  char input_short_str[4] = "0123";
  char *input_long_str = malloc(MAX_BUFFER_SIZE + 1);
  memset(input_long_str, 'a', MAX_BUFFER_SIZE + 1);
  FILE *long_file = fmemopen(input_long_str, MAX_BUFFER_SIZE + 1, "r");
  FILE *short_file = fmemopen(input_short_str, sizeof(input_short_str), "r");

  long long_size = get_buf_size(long_file);
  long short_size = get_buf_size(short_file);

  assert_int_equal(long_size, MAX_BUFFER_SIZE);
  assert_int_equal(short_size, sizeof(input_short_str));

  free(input_long_str);
  fclose(long_file);
  fclose(short_file);
  (void)state;
}

int main() {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(write_str_test),
    cmocka_unit_test(count_digraphs_test),
    cmocka_unit_test(file_size_test),
    cmocka_unit_test(get_buf_size_test),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
