// Copyright (c) 2022 Nikita Zheleztsov
//
// Distributed under the GNU GPLv3 software license, see the accompanying
// file LICENSE or visit <https://www.gnu.org/licenses/gpl-3.0.en.html>

#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>

#include "analyzer/analyzer.h"
#include "common/options.h"

static void serial_analyze_test(void **state) {
  char input[] = ":) dsjfids :( fdsi: jdsf )";
  FILE *input_file = fmemopen(input, sizeof(input), "r");
  options_t options = {.input = input_file, .output = NULL};
  analyzer_t analyzer = {
      .negative_num = 0, .positive_num = 0, .options = &options};

  int res = analyze(&analyzer);

  assert_int_equal(res, EXIT_SUCCESS);
  assert_int_equal(analyzer.positive_num, 1);
  assert_int_equal(analyzer.negative_num, 1);

  fclose(input_file);
  (void)state;
}

int main() {
  const struct CMUnitTest tests[] = {
      cmocka_unit_test(serial_analyze_test),
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
